/*
$.ajax('http://danit.com.ua/free-towns-list-json', {
    method:'GET',
    success: res => console.log(res)
});
 */
/*
document.body.classList.add('loading');
$
    .getJSON('http://danit.com.ua/free-towns-list-json')
    .done(res => {
        console.log(res);
    })
    .fail(err => {
        console.warn(err);
    });
*/
/*
const form = document.querySelector('form');
form.addEventListener('submit', event => {
    event.preventDefault();
    const {target: formEl} = event;

    const data = new FormData(formEl);
    const dataToSend = Object.fromEntries(data.entries());

    $.post(formEl.action, dataToSend)
        .then(res => console.log(res), err => console.warn('error'));
});
*/

class List {
    constructor(selector, dataUrl) {
        this._url = dataUrl;
        this._el = document.querySelector(selector);
        this._getData();
    }
    _initReloadButton() {
        const button = document.createElement('button');
        button.type = 'button';
        button.innerText = 'reload';
        button.addEventListener('click', this._reload.bind(this));
        this._el.after(button);
    }
    _reload() {

    }
    _getData() {
        this._el.classList.add('loading');
        this._el.innerHTML = '';
        $.getJSON(this._url)
            .done(this._onData.bind(this))
            .always(() => this._el.classList.remove('loading'))
    }
    _onData(users) {
        users.forEach(({name, age}) => {
            const li = document.createElement('li');
            li.innerText = `${name}: ${age}`;
            this._el.append(li);
        });
    }
}

const list1 = new List(
    'list1',
    'https://dan-it.herokuapp.com/list1'
);

const list2 = new List(
    'list2',
    'https://dan-it.herokuapp.com/list2'
);