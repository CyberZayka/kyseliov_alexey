/*
const user = {
    name: 'Alex',
    getName() {
        return this.name;
    }
};

const secondUser = {
    name: 'Someone'
};
secondUser.getName = user.getName;
console.log(secondUser.getName());
 */
/*
const listComponent = {
    list: [],
    loading: false,
    loadFilms(onLoaded) {
        this.loading = true;
        setTimeout((response) => {
            this.list = ['Film1',"Film2"];
            this.loading = false;
            onLoaded();
        }, 1000);
    }
};

renderFilms(listComponent.list);
listComponent.loadFilms(() => {
    renderFilms(listComponent.list);
});

function renderFilms(list) {
    console.log(list);
}
 */
/*
function func(suffix) {
    return this.name + suffix;
}

const user = {name: 'Test'};

console.log(func.call(user, '1k'));
console.log(func.apply(user, ['1k']));
*/
/*
function func(suffix) {
    return this.name + suffix;
}

const user = {name: 'Test'};

console.log(func.call(user, '1k'));

const boundFunc = func.bind({name: 'Bound name'},'1k');
console.log(boundFunc());
*/
/*
const user = {
    name: 'Alex',
    log() {
        console.log(this.name);
    }
};

user.log();
*/
/*
const app = {

        user: {
            firstName: '',
            lastName: '',
        },

        handleUserChange(property, event) {
            this.user[property] = event.target.value;
            console.log(this.user);
    }
};

const f = document.querySelector('#f');
const s = document.querySelector('#s');
f.addEventListener('input',app.handleUserChange.bind(app,'firstName'));
s.addEventListener('input',app.handleUserChange.bind(app,'lastName'));
*/
/*
function f(a) {
    return (b) => {
        return a + b;
    }
}

//console.log(f(3)(4));

const f2 = f(3);
console.log(f2(5));
*/

function sum(a, b, c) {
    return a + b + c;
}

function curry(original) {
    let args = [];
    const collect = (...partialArgs) => {
        args = [...args, ...partialArgs];

        if (args.length >= original.length) {
            return original(...args);
        }
        return collect;
    };

    return collect;
}

const sumCurried = curry(sum);

console.log(sumCurried((1),(2, 3)));


































































