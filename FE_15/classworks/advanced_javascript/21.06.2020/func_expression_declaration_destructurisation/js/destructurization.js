/*
let a = 1;
func();
a++;
setTimeout(func,1);
a++;
func();
a++;

function func() {
    console.log(a);
}
*/
/*
function createUser() {
    const name = 'Name';
    const age = 11;
    const prop = prompt('Prop name: ');
    const val = prompt('Val: ');

    const anotherUser = {name: 'other', legs: 2};
    return {
        name,
        age,
        children: [{name: 'Child1', age: 10}, {name: 'Child2', age: 7}],
        [prop + '___' + (2+3)]: val,
        ...anotherUser,
    };
}

const user = createUser();
const {name,age: num, children: [,{age}]} = user;

console.log(user);
console.log(age);
 */
/*
function sum({x = 0,y = 0}, {x: x2 = 0,y :y2 =0}) {
    return {
        x: x + x2,
        y: y + y2
    }
}

console.log(sum({x: 1, y: 2}, {x: 3, y: -5}));
*/

function sum() {
    return [...arguments].reduce(
        ({x,y}, {x: x1 = 0, y: y1 = 0}) => ({x: x + x1, y: y+y1}),
        {x: 0,y: 0});
}

console.log(sum({x: 1}, {y: 2}, {x: 3, y: -1}, {y: -10}));




















