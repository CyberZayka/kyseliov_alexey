
const initialApplicationState = {
    list:['item1','item2','item3'],
    list2:['item4','item5','item6'],
    menuOpened: false
};

class Component {
    //container;
    constructor(state) {
        this.state = state;
        this.container = this.createContainer();
    }
    createContainer() {
        //throw new Error('Write create container implementation');
        //console.warn('Write create container implementation');
        return document.createElement('div');
    }
    appendTo(parent) {
        parent.append(this.container);
    }
}

class Application extends Component {
    constructor(state) {
        super(state);

        const list = new List({list: state.list});
        list.appendTo(this.container);
        const onRemove = (data) => {
            state = {...state, list2: state.list2.filter(item => item !== data)};
        };
        const removableList = new RemovableList({list: state.list2, onRemove});
        removableList.appendTo(this.container);
    }
    createContainer() {
        const el = super.createContainer();
        el.classList.add('app');
        return el;
    }
}

class List extends Component {
    constructor(state) {
        super(state);
        const {list} = state;
        const items = this.createItems(state.list);
        items.forEach(item => {
            item.appendTo(this.container);
        });
    }
    createContainer() {
        const el = super.createContainer();
        el.classList.add('list');
        return el;
    }
    createItems(list) {
        return list.map(data => new ListItem({data}));
    }
}

class RemovableList extends List {
    createItems(list) {
        return list.map(data => new RemovableListItem({data, onRemove: this.state.onRemove}));
    }
}

class ListItem extends Component {
    constructor(state) {
        super(state);
        this.container.innerText = state.data;
    }
    createContainer() {
        const el = super.createContainer();
        el.classList.add('list-item');
        return el;
    }
}

class RemovableListItem extends ListItem {
    constructor(state) {
        super(state);
        const button = this.createRemovableButton();
        this.container.append(button);
    }
    createRemovableButton () {
        const button = document.createElement('button');
        button.innerText = 'remove';
        button.addEventListener('click',this.remove.bind(this));
        return button;
    }
    remove () {
        this.container.remove();
        this.state.onRemove(this.state.data);
    }

}




const app = new Application(initialApplicationState);
app.appendTo(document.body);