
//регулярка с инпутом (ДОМ)

/*
const numberRegex = /^(0|[1-9]\d*)([.,]\d+)?$/;
const input = document.querySelector('input');
input.addEventListener('input',onInput);

function onInput () {
    if (numberRegex.test(input.value)) {
        input.style.border = '1px solid green';
    }
    else {
        input.style.border = '1px solid red';
    }
}
 */

//класс с каунтером

/*
class Counter {
    constructor(initialValue) {
        this._createUI();
        this._listenButtons();
        this.updateValue(initialValue);
    }

    _createUI() {
        const container = document.createElement('div');
        const buttonDecrement = document.createElement('button');
        buttonDecrement.innerText = '-';
        const buttonIncrement = document.createElement('button');
        buttonIncrement.innerText = '+';
        const input = document.createElement('input');
        container.append(buttonDecrement);
        container.append(input);
        container.append(buttonIncrement);
        this._input = input;
        this._container = container;  //прокидаем контейнер, чтобы его можно было юзать извне
        this._buttonIncrement = buttonIncrement;
        this._buttonDecrement = buttonDecrement;
    }

    _listenButtons() {
        this._decrementBound = this.decrement.bind(this);
        this._buttonDecrement.addEventListener('click', this._decrementBound);
        this._incrementBound = this.increment.bind(this);
        this._buttonIncrement.addEventListener('click', this._incrementBound);
    }

    decrement() {
        this._input.value = (parseFloat(this._input.value) - 1).toString();
    }

    increment() {
        this._input.value = (parseFloat(this._input.value) + 1).toString();
    }

    updateValue(value) {
        this._input.value = value;
    }

    appendTo(parent) {
        parent.append(this._container);
    }

    destroy() {
        this._container.remove();
        this._buttonDecrement.removeEventListener('click',this._decrementBound);
        this._buttonIncrement.removeEventListener('click',this._incrementBound);
    }
}

for (let i = 0; i < 10; i ++) {
    const counter = new Counter(7);
    counter.appendTo(document.body);
}
 */