/*
const person = {
    name: 'Leshka',
    age: 21
};

const carFactory = function (payload = {}) {
    return {
        carName: 'Ford',
        carPrice: 2000,
        carYear: 1996,
            ...payload
    }
};

console.log(carFactory(person));
 */
/*
const maxValue = function (...arguments) {
    return Math.max(...arguments || []);
};

console.log(maxValue(1,2,3,56,34));
 */

const Animal = function (legs, yearOld) {
    this.legs = legs;
    this.yearOld = yearOld;
    this.move = function () {
        console.log('Animal is moving');
    };
    this.info = function () {
        console.log(`Animal is`, this.age, `years old`);
    };
};

const animalType = new Animal(2,0);

const Dog = function (name, color) {
    this.name = name;
    this.color = color;
};

Dog.prototype = animalType;

const dog1 = new Dog('Sharik','red');

const Bird = function (name, wingsLength) {
    this.name = name;
    this.wingslength = wingsLength;
};