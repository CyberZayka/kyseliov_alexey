//task 1
const citiesArr = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"];
//Возвращение элементов массивом с помощью деструктуризации и метода spread
const [...spreadCity] = citiesArr;
console.log(spreadCity);
//Возвращение элементов строками с помощью деструктуризации
const [kyiv,berlin,dubai,moscow,paris] = citiesArr;
console.log(kyiv,berlin,dubai,moscow,paris);

//task 2
const Employee = {
    name: 'Leshka',
    salary: 100000
};
const {name, salary} = Employee;
console.log(name, salary);

//task 3
const array = ['value', 'showValue'];
const [value, showValue] = array;
alert(value);
alert(showValue);