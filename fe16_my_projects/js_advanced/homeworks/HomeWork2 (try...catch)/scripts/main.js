
const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function makeListBooks(arrBooks) {

    const divRoot = document.querySelector('#root');
    const ul = document.createElement('ul');

    const listOfBooks = arrBooks.forEach(function (itemObj) {

        const li = document.createElement('li');

        const {author, name, price} = itemObj;

         if (author && name && price) {
             li.innerHTML = author + ' ' + name + ' ' + price;
             ul.appendChild(li);
             divRoot.appendChild(ul);
            }

        (function checkError() {
             const arrItem = [author, name, price];
             for (let checkElem of arrItem) {
                 try {
                     if (!checkElem) {
                         throw new SyntaxError(`Your data is incorrect in ${checkElem}`);
                     }
                 } catch (e) {
                     console.error(`You are wrong in ${arrItem}`);
                 }
             }
         })();

    })

}

makeListBooks(books);