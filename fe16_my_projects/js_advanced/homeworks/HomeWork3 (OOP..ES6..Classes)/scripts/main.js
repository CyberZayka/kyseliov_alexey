
class Employee {
    constructor({name, age, salary}) {
        this.initEmployee({name, age, salary});
    }

    initEmployee({name, age, salary}) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get workersInfo() {
        return `Worker's name is ${this._name}. His age is ${this._age}. He has earned ${this._salary} $`;
    }

    set workersInfo(editInfo) {
        [this.name, this.age, this.salary] = editInfo.split(' ');
    }

}

class Programmer extends Employee {
    constructor({name, age, salary, lang}) {
        super({name, age, salary});

        this.lang = lang;
    }

    get workersInfo () {
        return `Worker's name is ${this._name}. His age is ${this._age}. He has earned ${this._salary * 3} $`;
    }
}

const developer1 = new Programmer({
    name: 'Alexey Vladimirovich',
    age: 21,
    salary: 100000,
    lang: 'JS'
});

const developer2 = new Programmer({
    name: 'Ivan Ivanich',
    age: 40,
    salary: 120000,
    lang: 'PHP'
});

const developer3 = new Programmer({
    name: 'Petro Petrovich',
    age: 25,
    salary: 100000,
    lang: 'C#'
});

console.log(developer1, developer2, developer3);
